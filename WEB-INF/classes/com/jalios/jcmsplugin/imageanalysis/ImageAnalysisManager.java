package com.jalios.jcmsplugin.imageanalysis;

import java.io.File;
import java.net.URI;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.FileEntity;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

public class ImageAnalysisManager {
  private static final String API_KEY_HEADER = "Ocp-Apim-Subscription-Key";
  private static final String API_KEY = "ef3b2c01590a44eb96f560816827b65c";

  private static final String ANALYZE_URI = "https://westus.api.cognitive.microsoft.com/vision/v1.0/analyze";

  private static final ImageAnalysisManager SINGLETON = new ImageAnalysisManager();

  // --------------------------------------------------------
  // Singleton & init
  // --------------------------------------------------------
  private ImageAnalysisManager() {
    init();
  }

  public static ImageAnalysisManager getInstance() {
    return SINGLETON;
  }

  private void init() {

  }
  // --------------------------------------------------------
  // Functions
  // --------------------------------------------------------

  public boolean isAdultContent(File file) {
    HttpClient httpclient = HttpClients.createDefault();

    try {
      URIBuilder builder = new URIBuilder(ANALYZE_URI);
      builder.setParameter("visualFeatures", "Categories, Adult, Tags, Description, Faces, ImageType, Color");
      builder.setParameter("language", "en");
      builder.setParameter("details", "Celebrities");

      URI uri = builder.build();
      HttpPost request = new HttpPost(uri);
      request.setHeader(API_KEY_HEADER, API_KEY);

      // Set file on request
      request.setEntity(new FileEntity(file, ContentType.APPLICATION_OCTET_STREAM));

      HttpResponse response = httpclient.execute(request);
      HttpEntity entity = response.getEntity();

      if (entity != null) {
        System.out.println(EntityUtils.toString(entity));
      }
    } catch (Exception e) {
      System.out.println(e.getMessage());
    }
    return false;
  }

}
