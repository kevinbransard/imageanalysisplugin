package com.jalios.jcmsplugin.imageanalysis;

import java.io.File;
import java.util.Map;

import org.apache.log4j.Logger;

import com.jalios.jcms.BasicDataController;
import com.jalios.jcms.Data;
import com.jalios.jcms.FileDocument;
import com.jalios.jcms.Member;

public class ImageAnalysisDataController extends BasicDataController {
  private static final Logger LOGGER = Logger.getLogger(ImageAnalysisDataController.class);

  @Override
  public void afterWrite(Data data, int op, Member mbr, Map context) {
    FileDocument document = (FileDocument) data;

    File file = document.getFile();

    if (document.isImage() && ImageAnalysisManager.getInstance().isAdultContent(file)) {
      LOGGER.warn("IT IS AN ADULT CONTENT OMG !!!!");
    }

  }
}